var express = require('express');
var router = express.Router();



var orders = [
			{ id:1,name:"tommy",totale:10.00, data:"20-02-1016" },
			{ id:2,name:"tommy",totale:100.00, data:"20-02-1016" },
			{ id:3,name:"tommy",totale:99.00, data:"20-02-1016" },
			{ id:4,name:"tommy",totale:1.00, data:"20-02-1016" }, 
			{ id:5,name:"tommy",totale:4.00, data:"20-02-1016" },
			{ id:6,name:"tommy",totale:23.00, data:"20-02-1016" },
			{ id:7,name:"tommy",totale:57.60, data:"20-02-1016" },
			{ id:8,name:"tommy",totale:10.00, data:"20-02-1016" },
			{ id:9,name:"tommy",totale:10.00, data:"20-02-1016" },
			{ id:10,name:"tommy",totale:10.00, data:"20-02-1016" }];

/* POST order from particle */
router.get('/', function(req, res) {
    res.json(orders);
}).post('/',function(req, res) {
	if(!req.body.hasOwnProperty('id') || 
     !req.body.hasOwnProperty('name') ||
	 !req.body.hasOwnProperty('totale') ||
	 !req.body.hasOwnProperty('data')) {
    res.statusCode = 400;
    return res.send('Error 400: Post syntax incorrect.');
  } 
 
var newOrder = {
    id : req.body.id,
    name : req.body.name,
	totale: req.body.totale,
	data: req.body.data
	}; 
 
orders.push(newOrder);
res.json(true);
});

module.exports = router;